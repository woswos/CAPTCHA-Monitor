import os
import json
import shutil
import logging
from typing import Any, Set, Dict, List, Optional
from collections import defaultdict

import matplotlib.pyplot as plt
from jinja2 import Environment, FileSystemLoader
from sqlalchemy import case, func, literal_column
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import text

from captchamonitor.utils.config import Config
from captchamonitor.utils.models import Relay, Domain, FetchCompleted, AnalyzeCompleted


class Json:
    """
    Maps the Data into Json ready format
    """

    def __init__(
        self,
        rid: str = None,
        date_data: dict = None,
        url: str = None,
        country_data: dict = None,
    ) -> None:
        """
        Structuring in JSON format

        :param rid: Entry for relay_id
        :type rid: str
        :param date_data: Entry for the analyzed data and dates
        :type date_data: dict
        :param url: Entry for the url data
        :type url: str
        :param country_data: Entry for the county details
        :type country_data: dict
        """
        self.rid = rid
        self.date_data = date_data

        self.url = url
        self.country_data = country_data

        self.relay()

    def relay(self) -> Any:
        """
        Populates Relay with id and results or url and stats
        :return: relay details of the JSON
        """
        d: Dict[str, Any] = {}
        if self.date_data is not None:
            d["id"] = self.rid
            d["result"] = self.result()
        else:
            d["url"] = self.url
            d["stats"] = self.result()
        return str(d).replace("'", '"')

    def result(self) -> List[Any]:
        """
        Populates the inner list of result in JSON data
        :return: List of results (Date and Data) or (country, data)
        """
        list_res: List[Any] = []
        temp: Dict[Any, Any] = {}
        if self.date_data is not None:
            for i in self.date_data:
                temp["date"] = i
                temp["data"] = self.date_data[i][0]
                temp["tor_block"] = self.date_data[i][1]
                temp["partial_block"] = self.date_data[i][2]
                temp["all_block"] = self.date_data[i][3]
                temp["no_block"] = self.date_data[i][4]
                temp["all_websites"] = self.date_data[i][5]

                list_res.append(temp)
                temp = {}
        if self.country_data is not None:
            for i in self.country_data:
                temp["country"] = i
                temp["data"] = []
                for j in self.country_data[i]:
                    t = {}
                    t["id"] = j
                    t["time"] = self.country_data[i][j]
                    temp["data"].append(t)
                list_res.append(temp)
                temp = {}
        return list_res


class RenderDashboard:
    """
    Renders the latest version of the dashboard and exports the HTML files to
    www folder
    """

    def __init__(self, config: Config, db_session: sessionmaker) -> None:
        """
        Initializes the dashboard renderer

        :param config: The config class instance that contains global configuration values
        :type config: Config
        :param db_session: Database session used to connect to the database
        :type db_session: sessionmaker
        """
        # Private class attributes
        self.__logger = logging.getLogger(__name__)
        self.__config: Config = config
        self.__db_session: sessionmaker = db_session
        self.__template_location: str = os.path.join(
            self.__config["dashboard_location"], "templates"
        )
        self.__jinja_environment = Environment(
            loader=FileSystemLoader(self.__template_location)
        )
        # self.graph_name: List[str] = []
        self.graph_name: Dict[Any, List] = defaultdict(list)
        # self.global_fingerprint = []
        self.global_fingerprint: Set[str] = set()
        self.image_name: List[str] = []
        self.graph_string: Optional[Any] = []

        self.data_for_graph: Dict[Any, Any] = self.prepare_data_for_graph()
        # Clean the www directory
        self.cleanup_www_folder()
        # Get hold of the docstrings
        self.graph_string.append(self.graph_for_tor_block.__doc__)
        self.graph_string.append(self.graph_for_tor_partial_block.__doc__)
        self.graph_string.append(self.graph_for_both_block.__doc__)
        self.graph_string.append(self.graph_for_tor_none_block.__doc__)
        # Render all pages
        self.render_index()
        self.data_for_country: Dict[Any, Any] = self.prepare_data_for_country()
        # self.render_dashboard()
        self.render_domain_list()
        # Render graph: blocked websites for Tor
        self.graph_for_tor_block()
        # Render graph: partially blocked websites for Tor
        self.graph_for_tor_partial_block()
        # # Render graph: blocked websites for both Tor and non-Tor nodes
        self.graph_for_both_block()
        # # Render graph: accessible websites for Tor
        self.graph_for_tor_none_block()
        # # Export graph to the website
        self.export_graph()
        self.render_dashboard()

    def __write_to_file(self, filename: str, html_data: str) -> None:
        """
        Writes given data to the specified file inside the www folder

        :param filename: Name of the HTML file to export
        :type filename: str
        :param html_data: HTML data to write to the file
        :type html_data: str
        """
        html_file_path = os.path.join(self.__config["dashboard_www_location"], filename)
        with open(html_file_path, "w", encoding="UTF-8") as file:
            file.write(html_data)

    def cleanup_www_folder(self) -> None:
        """
        Remove contents of the www folder and copy the static folder again
        """
        self.__logger.debug("Cleaning up the www folder")

        www_location = self.__config["dashboard_www_location"]
        static_location = os.path.join(self.__config["dashboard_location"], "static")

        # Remove folders and files inside the www folder
        for filename in os.listdir(www_location):
            file_path = os.path.join(www_location, filename)
            if (os.path.isfile(file_path) or os.path.islink(file_path)) and (
                filename != ".gitignore"
            ):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)

        # Copy static files
        shutil.copytree(static_location, www_location, dirs_exist_ok=True)

    def render_index(self, filename: str = "index.html") -> None:
        """
        Render the index page, which visitors first see

        :param filename: Name of the HTML file to export, defaults to "index.html"
        :type filename: str
        """
        self.__logger.debug("Rendering %s", filename)

        template = self.__jinja_environment.get_template("index.html")
        html_data = template.render()

        self.__write_to_file(filename=filename, html_data=html_data)

    def render_domain_list(self, filename: str = "domain_list.html") -> None:
        """
        Render the page that contains the list of tracked domains

        :param filename: Name of the HTML file to export, defaults to "domain_list.html"
        :type filename: str
        """
        self.__logger.debug("Rendering %s", filename)

        data = self.__db_session.query(Domain).all()

        template = self.__jinja_environment.get_template("domain_list.html")
        html_data = template.render(data=data)

        self.__write_to_file(filename=filename, html_data=html_data)

    # pylint: disable=R0914,R0912
    def prepare_data_for_country(self) -> Dict[Any, Any]:
        """
        This function returns data for relays according to the different websites. Hence, tries to show data according to individual websites

        :return: JSON data according to the url.
        """
        relay_id_info = (
            self.__db_session.query(FetchCompleted.relay_id)
            .join(
                AnalyzeCompleted,
                # pylint: disable=W0143
                AnalyzeCompleted.fetch_completed_id == FetchCompleted.id,
            )  # pylint: disable=W0143
            .filter(FetchCompleted.fetcher_id == 2)
            .group_by(FetchCompleted.relay_id)
            .all()
        )
        country_relay = defaultdict(list)
        # Maps country codes to the relay id
        for relay in relay_id_info:
            info = (
                self.__db_session.query(Relay.id, Relay.country)
                .filter(Relay.id == relay[0])
                .all()
            )
            # cursor.execute(f"select r.id,r.country from relay r where r.id = {i[0]}")
            country_relay[info[0][1]].append(info[0][0])

        relay_date = (
            self.__db_session.query(
                FetchCompleted.relay_id, func.date(FetchCompleted.created_at)
            )
            .join(
                AnalyzeCompleted,
                AnalyzeCompleted.fetch_completed_id  # pylint: disable=W0143
                == FetchCompleted.id,
            )
            .filter(FetchCompleted.fetcher_id == 2)  # pylint: disable=W0143
            .group_by(FetchCompleted.relay_id)
            .group_by(func.date(FetchCompleted.created_at))
            .all()
        )

        # Maps the relay id to the date and also updates details of blocking with integer values
        relay_date_dict: Dict[Any, Any] = defaultdict(dict)
        for _, i in enumerate(relay_date):
            date = f"{i[1].year}-{i[1].month}-{i[1].day}"
            relay_date_dict[i[0]][date] = str(_)

        url_country = (
            self.__db_session.query(FetchCompleted.url, Relay.country)
            .join(Relay, FetchCompleted.relay_id == Relay.id)  # pylint: disable=W0143
            .group_by(FetchCompleted.id)
            .group_by(Relay.country)
            .all()
        )

        # Maps url to country codes
        url_country_dict = defaultdict(list)
        for i in url_country:
            url_country_dict[i[0]].append(i[1])

        # Maps date of specific relay ids to the country codes. Like: {country:{relay_id:{date:block}}}
        country_id_date: Dict[Any, Any] = defaultdict()
        for key in country_relay:
            country_id_date[key] = defaultdict(list)
            for value in country_relay[key]:
                country_id_date[key][value] = relay_date_dict[value]
        try:
            # Dictionary form of: {url:{country:{relay_id:{date:block}}}}
            json_data: Dict[Any, Any] = defaultdict(list)
            for key in url_country_dict:
                json_data[key] = {}
                for value in url_country_dict[key]:
                    json_data[key][value] = dict(country_id_date[value])
        except KeyError as e:
            self.__logger.debug(
                "Analyzer needs to update results from Fetchcompleted. Mismatch because of: %s",
                e,
            )

        case_block = case(
            [
                (AnalyzeCompleted.status_check == 0, literal_column("'1'")),
                (AnalyzeCompleted.dom_analyze == 0, literal_column("'1'")),
                (AnalyzeCompleted.consensus_lite_dom == 1, literal_column("'1'")),
                (AnalyzeCompleted.captcha_checker == 1, literal_column("'2'")),
                (
                    AnalyzeCompleted.consensus_lite_captcha.in_([4, 5, 6]),
                    literal_column("'2'"),
                ),
            ],
            else_=literal_column("'0'"),
        )
        get_block_info = (
            self.__db_session.query(
                FetchCompleted.url,
                FetchCompleted.relay_id,
                func.date(FetchCompleted.created_at),
                case_block,
            )
            .join(
                AnalyzeCompleted,
                AnalyzeCompleted.fetch_completed_id  # pylint: disable=W0143
                == FetchCompleted.id,  # pylint: disable=W0143
            )
            .filter(FetchCompleted.fetcher_id == 2)  # pylint: disable=W0143
            .group_by(FetchCompleted.url)
            .group_by(FetchCompleted.relay_id)
            .group_by(func.date(FetchCompleted.created_at))
            .group_by(text("anon_1"))
            .all()
        )
        # Replace the value of blocking with the correct data
        for _ in get_block_info:
            country = (
                self.__db_session.query(Relay.country).filter(Relay.id == _[1]).first()
            )
            url, rel, dt, block = _
            date = f"{dt.year}-{dt.month}-{dt.day}"
            json_data[url][country[0]][rel][date] = block

        # Replace the relay_id with relay_fingerprint
        for i in list(json_data):
            for j in list(json_data[i]):
                for k in list(json_data[i][j]):
                    fingerprint = (
                        self.__db_session.query(Relay.fingerprint)
                        .filter(Relay.id == k)
                        .first()
                    )
                    json_data[i][j][fingerprint[0]] = json_data[i][j][k]
                    del json_data[i][j][k]

        concat_json = ""
        for key in json_data:
            a = Json(url=key, country_data=json_data[key])
            concat_json += f"{(a.relay())},"
            new_string = f'"website_relay" : [{concat_json[:-1]}]'
            new_string = "{" + new_string + "}"

        self.__logger.info("JSON data:  %s ", new_string)
        return json.loads(new_string)

    # pylint: disable=R0914,R0915
    def prepare_data_for_graph(self) -> Dict[Any, Any]:
        """
        Synthesize Data for different graphs.

        :return: JSON data to generate the graph. Like Scores for y axis, Timestamps for x axis, Total query, Obtained Query, Relay Fingerprint
        """
        data_point = (
            self.__db_session.query(
                func.count(AnalyzeCompleted.id),
                func.date(AnalyzeCompleted.created_at),
                FetchCompleted.relay_id,
            )
            .join(
                # pylint: disable=W0143
                FetchCompleted,
                AnalyzeCompleted.fetch_completed_id == FetchCompleted.id,
            )
            .group_by(func.date(AnalyzeCompleted.created_at))
            .group_by(FetchCompleted.relay_id)
            .order_by(FetchCompleted.relay_id)
            .order_by(func.date(AnalyzeCompleted.created_at))
            .all()
        )

        time: Any = []
        relay_id: List[str] = []
        relay_data_test: Dict[Any, Dict] = defaultdict(dict)

        for k, y in enumerate(data_point):
            date = f"{y[1].year}-{y[1].month}-{y[1].day}"
            relay_id.append(y[2])
            if relay_data_test[relay_id[k]] == {}:
                timestamp_info: Dict[Any, List] = defaultdict(list)
            timestamp_info[date] = []
            relay_data_test[relay_id[k]] = dict(timestamp_info)

        fingerprint_set: Set[Any] = set()
        time_set: Set[Any] = set()
        for key, time in relay_data_test.items():
            fingerprint = (
                self.__db_session.query(Relay.fingerprint).filter(Relay.id == key).all()
            )
            fingerprint = fingerprint[0][0]
            for time_ in time.keys():
                work_data: List[Any] = []
                all_data: List[Any] = []
                analyzed_data = (
                    self.__db_session.query(FetchCompleted.url)
                    .join(
                        # pylint: disable=W0143
                        AnalyzeCompleted,
                        AnalyzeCompleted.fetch_completed_id == FetchCompleted.id,
                    )
                    .filter(FetchCompleted.relay_id == key)  # pylint: disable=W0143
                    .filter(func.date(AnalyzeCompleted.created_at) == time_)
                    .all()
                )

                fingerprint_set.add(fingerprint)
                time_set.add(time_)

                # Test
                test_data = (
                    self.__db_session.query(FetchCompleted.url)
                    .join(
                        AnalyzeCompleted,
                        AnalyzeCompleted.fetch_completed_id  # pylint: disable=W0143
                        == FetchCompleted.id,
                    )
                    .filter(FetchCompleted.relay_id == key)  # pylint: disable=W0143
                    .filter(func.date(AnalyzeCompleted.created_at) == time_)
                    .filter(AnalyzeCompleted.status_check == 0)
                    .all()
                )

                tor_block_url = [i[0] for i in test_data]

                test_data = (
                    self.__db_session.query(FetchCompleted.url)
                    .join(
                        AnalyzeCompleted,
                        AnalyzeCompleted.fetch_completed_id  # pylint: disable=W0143
                        == FetchCompleted.id,
                    )
                    .filter(FetchCompleted.relay_id == key)  # pylint: disable=W0143
                    .filter(func.date(AnalyzeCompleted.created_at) == time_)
                    .filter(AnalyzeCompleted.status_check == 1)
                    .all()
                )

                all_block_url = [i[0] for i in test_data]

                test_data = (
                    self.__db_session.query(FetchCompleted.url)
                    .join(
                        AnalyzeCompleted,
                        AnalyzeCompleted.fetch_completed_id  # pylint: disable=W0143
                        == FetchCompleted.id,
                    )
                    .filter(FetchCompleted.relay_id == key)  # pylint: disable=W0143
                    .filter(func.date(AnalyzeCompleted.created_at) == time_)
                    .filter(AnalyzeCompleted.dom_analyze.in_([1, 4]))
                    .all()
                )
                no_block_url = [i[0] for i in test_data]

                test_data = (
                    self.__db_session.query(FetchCompleted.url)
                    .join(
                        AnalyzeCompleted,
                        AnalyzeCompleted.fetch_completed_id  # pylint: disable=W0143
                        == FetchCompleted.id,
                    )
                    .filter(FetchCompleted.relay_id == key)  # pylint: disable=W0143
                    .filter(func.date(AnalyzeCompleted.created_at) == time_)
                    .filter(AnalyzeCompleted.dom_analyze == 0)
                    .all()
                )
                partial_block_url = [i[0] for i in test_data]

                work_data.append(
                    round(len(tor_block_url) / len(analyzed_data) * 100, 2)
                )
                work_data.append(
                    round(len(partial_block_url) / len(analyzed_data) * 100, 2)
                )
                work_data.append(
                    round(len(all_block_url) / len(analyzed_data) * 100, 2)
                )
                work_data.append(round(len(no_block_url) / len(analyzed_data) * 100, 2))
                work_data.append(len(analyzed_data))

                analyzed_data_url = [i[0] for i in analyzed_data]

                all_data.append(work_data)
                all_data.append(tor_block_url)
                all_data.append(partial_block_url)
                all_data.append(all_block_url)
                all_data.append(no_block_url)
                all_data.append(analyzed_data_url)

                relay_data_test[key][time_] = all_data  # pylint: disable=R1733

        add = []
        for i in relay_data_test.keys():
            fingerprint = (
                self.__db_session.query(Relay.fingerprint).filter(Relay.id == i).all()
            )
            y = (fingerprint[0][0], i)
            add.append(y)

        # Replace relay id with relay_fingerprint
        for i in add:
            relay_data_test[i[0]] = relay_data_test.pop(i[1])

        concat_json = ""
        new_string = ""

        # Create JSON data
        for key in relay_data_test:
            a = Json(rid=key, date_data=relay_data_test[key])
            concat_json += f"{(a.relay())},"
            new_string = f'"relay_id" : [{concat_json[:-1]}]'
            new_string = "{" + new_string + "}"

        self.__logger.info("JSON data:  %s ", new_string)
        return json.loads(new_string)

    def render_graph_new(
        self,
        kth: int,
        fingerprint: str,
        data_for_graph: Dict[Any, Any],
        ylabel: str,
        title: str,
        graph_type: int,
        color: str,
    ) -> None:
        """
        Renders the graph.

        :param kth: Gets the detail of K th Relay_id
        :type kth: int
        :param fingerprint: The fingerprint taken into consideration
        :type fingerprint: str
        :param data_for_graph: Details of all Relays with different timestamp and the analyzed data
        :type data_for_graph: (Dict[Any, Any])
        :param ylabel: ylabel for the Graph
        :type ylabel: str
        :param title: Title of the Graph
        :param graph_type: The type of graph one wants to associate the plot with (Block, partial block, all block, none blocked)
        :type graph_type: int
        :param color: color for the graph to be generated
        :type color: str
        """
        time = []
        data = []
        websites = 0
        x = len(data_for_graph["relay_id"][kth]["result"])
        for i in range(x):
            # x axis
            time.append(data_for_graph["relay_id"][kth]["result"][i]["date"])
            data.append(
                data_for_graph["relay_id"][kth]["result"][i]["data"][graph_type]
            )
            websites += data_for_graph["relay_id"][kth]["result"][i]["data"][4]

        fig = plt.figure(figsize=(20, 10))  # pylint: disable=W0612
        plt.ylim(top=100)
        plt.bar(time, data, width=0.25, color=color)
        plt.xlabel("Timestamp in days")

        plt.ylabel(ylabel)
        plt.title(title)
        txt_str = f"No of websites that has been evaluated: {websites/x}"
        plt.figtext(0.02, 0.035, txt_str)
        plt.rcParams.update({"font.size": 20})
        plt.rc("xtick", labelsize=26)
        plt.rc("ytick", labelsize=26)

        images_location = os.path.join(
            self.__config["dashboard_location"], "static/images/"
        )
        plt.savefig(images_location + f"relay_{fingerprint}_{graph_type}.png")
        self.graph_name[fingerprint].append(f"relay_{fingerprint}_{graph_type}.png")
        plt.close()

    def graph_for_tor_block(self) -> None:
        """
        Basic plot for the percentage of websites blocking the given Tor exit relay.
        """
        title = "Checks the percentage of blocked websites"
        ylabel = "Percentage where Tor is blocked (status_check) (%)"
        length = len(self.data_for_graph["relay_id"])
        for i in range(0, length):
            fingerprint = self.data_for_graph["relay_id"][i]["id"]
            self.global_fingerprint.add(fingerprint)
            self.__logger.debug("%s [relay: %s ]", title, fingerprint)
            self.render_graph_new(
                i,
                fingerprint,
                self.data_for_graph,
                ylabel,
                title,
                graph_type=0,
                color="maroon",
            )

    def graph_for_tor_partial_block(self) -> None:
        """
        Basic graph for the percentage of websites partially blocking tor for the given Tor exit relay.
        """
        title = "Checks the percentage of websites partially blocking tor nodes"
        ylabel = "Percentage where Tor partially-blocked percentage\n (dom_analyze) (%)"
        length = len(self.data_for_graph["relay_id"])
        for i in range(0, length):
            fingerprint = self.data_for_graph["relay_id"][i]["id"]
            self.global_fingerprint.add(fingerprint)
            self.__logger.debug("%s [relay: %s ]", title, fingerprint)
            self.render_graph_new(
                i,
                fingerprint,
                self.data_for_graph,
                ylabel,
                title,
                graph_type=1,
                color="orange",
            )

    def graph_for_both_block(self) -> None:
        """
        Contains the basic graph for the percentage of websites blocking both Tor and Control Nodes
        """
        title = "Checks the percentage of websites:\n blocking both the control nodes and tor nodes"
        ylabel = "Percentage of websites: \n Both control nodes & tor are blocked\n (status_check) (%)"
        length = len(self.data_for_graph["relay_id"])
        for i in range(0, length):
            fingerprint = self.data_for_graph["relay_id"][i]["id"]
            self.global_fingerprint.add(fingerprint)
            self.__logger.debug("%s [relay: %s ]", title, fingerprint)
            self.render_graph_new(
                i,
                fingerprint,
                self.data_for_graph,
                ylabel,
                title,
                graph_type=2,
                color="yellow",
            )

    def graph_for_tor_none_block(self) -> None:
        """
        Contains the basic graph for the percentage of websites that let's Tor exit relay access them.
        """
        title = "Checks the percentage of websites unblocked by Tor"
        ylabel = "Percentage:\n Websites unblocked by Tor (%)"
        length = len(self.data_for_graph["relay_id"])
        for i in range(0, length):
            fingerprint = self.data_for_graph["relay_id"][i]["id"]
            self.global_fingerprint.add(fingerprint)
            self.__logger.debug("%s [relay: %s ]", title, fingerprint)
            self.render_graph_new(
                i,
                fingerprint,
                self.data_for_graph,
                ylabel,
                title,
                graph_type=3,
                color="green",
            )

    def export_graph(self, filename: str = "reanalyze.html") -> None:
        """
        Exports the graphs to the html file

        :param filename: Name of the HTML file to export, defaults to "reanalyze.html"
        :type filename: str
        """
        # create an empty webpage with relay.html
        template = self.__jinja_environment.get_template(filename)

        for _, i in enumerate(list(self.global_fingerprint)):
            html_data = template.render(
                fingerprint=i, data=self.graph_name[i], string=self.graph_string
            )
            self.__write_to_file(filename=f"{i}.html", html_data=html_data)

    def render_dashboard(self, filename: str = "dashboard.html") -> None:
        """
        Render the index page, which visitors first see

        :param filename: Name of the HTML file to export, defaults to "index.html"
        :type filename: str
        """
        self.__logger.debug("Rendering %s", filename)

        template = self.__jinja_environment.get_template("dashboard.html")
        html_data = template.render(
            data1=self.data_for_graph, data2=self.data_for_country
        )

        self.__write_to_file(filename=filename, html_data=html_data)
